package optimise;

import messages.errorSuccess;

import java.awt.*;
import javaQuery.j2ee.ImageResize;
import java.awt.image.*;
import java.io.*;
import java.net.URISyntaxException;
import javax.imageio.*;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class optimisation {

    private Boolean success;
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_PURPLE = "\u001B[35m";


    public void compress(String path, String newName) {

        try {
            ImageResize ir = new ImageResize();
            ir.compressImage(path, newName, 1280, 720);

            // This will be used to ensure a success message shows ONLY IF no exception is thrown
            success = true;

        } catch (Exception e) {
            errorOrSuccessMessage(2, null, e, null);
        }

        if (success) { // if success is TRUE (meaning no exceptions have occurred)
            // the message is a type 1 (all went well a.k.a success)
            String message = "The image has been compressed. Click 'View file' or 'View folder' to view.";
            errorOrSuccessMessage(1, message, null, newName);
        } else { // if success is FALSE (meaning an exception has occurred)
            // the message is a type 0 (an error occured)
            String message = "An error occurred while compressing the image, try again!";
            errorOrSuccessMessage(0, message, null, null);
        }

    }


    // Gets error or success message for display on dialog
    private void errorOrSuccessMessage(int type, String message, Exception e, String link) {
        errorSuccess errorSuccessObj = new errorSuccess();
        errorSuccessObj.getMessage(type, message, e, link);
    }


    // method to watermark image
    public String watermark(int type, String path, String newName) {

        File newResized;
        BufferedImage resizedWatermark;

        try {

            File fileToWatermark = new File(path);
            System.out.println(ANSI_PURPLE + "The image to optimise: " + ANSI_RESET + ANSI_GREEN + fileToWatermark + ANSI_RESET);

            BufferedImage imageToWatermark = ImageIO.read(fileToWatermark);
            BufferedImage watermarkImage = ImageIO.read(getClass().getResource("/media/watermark.jpg"));

            // This sets the height and width to use when resizing
            BufferedImage resized = resize(watermarkImage, imageToWatermark.getHeight(), imageToWatermark.getWidth());

            // This saves the resized watermark image
            newResized = new File(".watermark-resized.png");
            System.out.println(ANSI_PURPLE + "The stretched watermark: " + ANSI_RESET + ANSI_GREEN + newResized + ANSI_RESET);
            ImageIO.write(resized, "png", newResized);

            //re-assign the watermark to the new image
            resizedWatermark = ImageIO.read(newResized);

            // initializes necessary graphic properties
            Graphics2D g2d = (Graphics2D) imageToWatermark.getGraphics();
            AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.4f);
            g2d.setComposite(alphaChannel);

            // paints the image watermark
            g2d.drawImage(imageToWatermark, 0, 0, null);
            g2d.drawImage(resizedWatermark, 0, 0, null);

            File newImagePath = new File(newName);
            ImageIO.write(imageToWatermark, "jpg", newImagePath);
            g2d.dispose();

            System.out.println("The image watermark is added to the image.");

            // Delete the stretched new watermark
            Files.deleteIfExists(Paths.get(newResized.toURI()));

            // This will be used to ensure a success message shows ONLY IF no exception is thrown
            success = true;

        } catch (Exception ex) {
            errorOrSuccessMessage(2, null, ex, null);
        }
        if (success) { // if success is TRUE (meaning no exceptions have occurred)
            // the message is a type 1 (all went well a.k.a success)
            String message = "The image has been watermarked. Click 'View file' or 'View folder' to view.";
            errorOrSuccessMessage(1, message, null, newName);
        } else { // if success is FALSE (meaning an exception has occurred)
            // the message is a type 0 (an error occured)
            String message = "An error occurred while watermarking the image, try again!";
            errorOrSuccessMessage(0, message, null, null);
        }

        if (type == 2) {
            return newName;
        } else {
            return null;
        }

    }


//  RESIZES THE WATERMARK WITH REFERENCE TO THE IMAGES TO BE WATERMARKED
    private static BufferedImage resize(BufferedImage img, int height, int width) {

        Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return resized;

    }


}


